package com.example.soundlooper;

import java.io.File;
import android.media.*;

public class SamplePlayer /*extends Thread*/ {

	int taktSchlag; //on which beat the clip is played
//	File file;
	MediaPlayer clip;
//	AudioInputStream ais;

	SamplePlayer(String soundFilePath, int t){
		this.taktSchlag = t;
		if(soundFilePath != null){
//		    this.file = new File("sounds/"+fileName);
			try {
				this.clip = new MediaPlayer();
				this.clip.setDataSource(soundFilePath);
				this.clip.prepare();
				
				
//                ais = AudioSystem.getAudioInputStream( file );
//                AudioFormat format = ais.getFormat();
//                DataLine.Info info = new DataLine.Info(Clip.class, format);

//                clip = (Clip)AudioSystem.getLine(info);
//                clip.open(ais);
			
				System.out.println("clip length = "+(this.clip.getDuration()/1000) );
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	public void playSound(){
		if(this.clip != null){
//            if(clip.isRunning()) System.out.println("Clip still running");//not good, would mean that a clip is longer than the complete loop
//            if(clip.isPlaying()) 
//            	{this.clip.stop();}
//			this.clip.seekTo(0);  // Must always rewind!
        	this.clip.start();
		}
	}
	public int getTaktSchlag(){
		return this.taktSchlag;
	}

	//public void run(){;}
}
