package com.example.soundlooper;

import java.util.LinkedList;

public class SoundGenerator {

	public static void main(String[] args) {
		new SoundGenerator();
	}
	public SoundGenerator() {
		//String[] files = {"bassDrum.wav","snare3.wav","hihat2.wav",null}; 
		String[] files = {"bassDrum.wav","hihat2.wav",null,"hihat2.wav",null,"snare3.wav","hihat2.wav",null,"hihat2.wav",null}; 
		int taktSchlag[] = {1,1,2,3,4,5,5,6,7,8};
		int viertel = 8;
		int duration = 800;//seconds in milli-seconds

		//String[] files = {"bassDrum.wav","hihat2.wav","bassDrum.wav"}; 
		//int beat[] = {1,2,3};
		//int beats = 8;
		//int duration = 800;//seconds in milli-seconds

		LinkedList<SamplePlayer> clipLoop = new LinkedList<SamplePlayer>();
		for(int i = 0; i < files.length; i++){
//			System.out.println();
			SamplePlayer sp = new SamplePlayer("temp/sounds/" +files[i],taktSchlag[i]);
			//sp.start();
			clipLoop.add(sp);
		}

		Looper looper = new Looper(viertel, duration, clipLoop);
		//looper.loop();
		looper.start();
	}
}
