
import java.util.LinkedList;

public class Looper extends Thread {

	int viertel;//schläge per takt
	int duration;//in milli-seconds
	int idleTime;//time to sleep until call of the next samplePlayer
	LinkedList<samplePlayer> playerList;

	int iterator = 0;
	int taktSchlag = 1;

	Looper(int v, int d, LinkedList<samplePlayer> list){
		viertel = v;
		duration = d;
		idleTime = d/(v);
		//System.out.println("BPM = "+(60000/(4*idleTime)));
		System.out.println("Dauer per Viertel-Note = "+idleTime +" ms");	
		playerList = list;
	}

	public void samplePlayNext(){
		do {
			playerList.get(iterator).playSound();
			iterator++;
			if(playerList.size() == iterator) iterator = 0;
		} while(playerList.get(iterator).getTaktSchlag() == taktSchlag);
	}

	/*public void loop(){
		long now = 0;
		long triggerTime = 0;
		while(true){
			now = System.currentTimeMillis();
			triggerTime = now+idleTime;
			while(now < triggerTime){
				now = System.currentTimeMillis();
			}

			samplePlayNext();
			taktSchlag++;
			if(taktSchlag > beats) taktSchlag = 1;
		}
	}*/

	public void run(){
		while (true) {
    		samplePlayNext();
			taktSchlag++;
			if(taktSchlag > viertel) taktSchlag = 1;
			try {
				this.sleep(idleTime); // Wait idleTime milli-seconds
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
