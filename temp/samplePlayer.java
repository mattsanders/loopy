
import java.io.File;
import javax.sound.sampled.*;

public class samplePlayer /*extends Thread*/ {

	int taktSchlag; //on which beat the clip is played
	File file;
	Clip clip;
	AudioInputStream ais;

	samplePlayer(String fileName, int t){
		taktSchlag = t;
		if(fileName != null){
		    file = new File("sounds/"+fileName);
			try {
                ais = AudioSystem.getAudioInputStream( file );
                AudioFormat format = ais.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, format);

                clip = (Clip)AudioSystem.getLine(info);
                clip.open(ais);

				System.out.println("clip length = "+(clip.getMicrosecondLength()/1000) );
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	public void playSound(){
		if(clip != null){
            if(clip.isRunning()) System.out.println("Clip still running");//not good, would mean that a clip is longer than the complete loop
            clip.stop();//if it is still running: stop it
            clip.setFramePosition(0);  // Must always rewind!
        	clip.start();
		}
	}
	public int getTaktSchlag(){
		return taktSchlag;
	}

	//public void run(){;}
}
